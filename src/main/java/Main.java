import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String[] args) {

        Human child1 = new Man("Nazrin", "Salimli");
        child1.setYear(1999);
        Human child2 = new Man("Kamil", "Salimov");
        child2.setYear(2010);
        Human child3 = new Man("Meyryem", "Kerimova");
        child3.setYear(2001);
        Human child33 = new Man("Resul", "Kerimov");
        child33.setYear(2005);
        Human woman = new Woman("Saida", "Salimova");
        Human man = new Man("Mammad", "Salimov");
        Family family = new Family(woman, man);
        family.addChild(child1);

        Human woman1 = new Woman("Sitara", "Mammadova");
        Human man1 = new Man("Farman", "Mammadov");
        Family family1 = new Family(woman1, man1);
        family1.addChild(child2);

        Human woman2 = new Woman("Afaq", "Salimova");
        Human man2 = new Man("Vasif", "Salimov");
        Family family2 = new Family(woman2, man2);
        family2.addChild(child3);
        family2.addChild(child33);

        Human woman3 = new Woman("Ulker", "Kerimova");
        Human man3 = new Man("Shahin", "Kerimov");
        Family family3 = new Family(woman3, man3);


        System.out.println(family.countFamily());
        System.out.println(family1.countFamily());
        System.out.println(family2.countFamily());
        System.out.println(family3.countFamily());
        System.out.println();


        FamilyDao familyDao = new CollectionFamilyDao();
        familyDao.saveFamily(family);
        familyDao.saveFamily(family1);
        familyDao.saveFamily(family2);
        familyDao.saveFamily(family3);


        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);

        familyController.displayAllFamilies();
        System.out.println();

        familyController.getFamiliesBiggerThan(3);
        System.out.println();

        familyController.getFamiliesLessThan(5);
        System.out.println();

        familyController.countFamiliesWithMemberNumber(3);

        Human woman4 = new Woman("Farida", "Kerimova");
        Human man4 = new Man("Cavid", "Kerimov");

        familyController.createNewFamily(woman4, man4);
        System.out.println();

        familyController.displayAllFamilies();
        familyController.deleteFamily(5);
        familyController.displayAllFamilies();
        System.out.println();

        familyController.bornChild(family, "Camil", "Xedice");
        System.out.println(family);
        System.out.println();

        familyController.adoptChild(family, new Man("Camal", "Cavadzade"));
        System.out.println(family.getChildren());
        System.out.println();


        System.out.println(familyController.getAllFamilies());
        familyController.deleteAllChildrenOlderThen(20);
        System.out.println(familyController.getAllFamilies());


        familyController.displayAllFamilies();
        System.out.println();
        System.out.println(familyController.count());


        familyController.displayAllFamilies();
        System.out.println(familyController.getFamilyById(3));

        Set<Pet> pets = new HashSet<>();
        Pet pet = new Dog();
        Pet pet1 = new Cat();
        Pet pet2 = new Mouse();
        pets.add(pet);
        pets.add(pet1);
        pets.add(pet2);


        family.setPet(pets);
        System.out.println(familyController.getPets(1));
        System.out.println(familyController.getFamilyById(1));


        familyController.addPet(1, pet2);
        System.out.println(familyController.getFamilyById(1));


    }
}

