import java.util.Map;

public final class Woman extends Human {
    public Woman() {
    }

    public Woman(String name, String surname) {
        super(name, surname);
    }

    public Woman(String name, String surname, int year, int iq, Map<String, String> weekNotes) {
        super(name, surname, year, iq, weekNotes);
    }

    void makeup() {
        System.out.println("подкраситься");
    }

    @Override
    public void greetPet(Pet pet) {
        System.out.println("Привет, " + pet.getNickname());
    }

}
